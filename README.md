# Baby VS Cat

Développer un jeu où il faut intéragir avec un bébé qui pleure
Style animal virtuel (tamagotchi)

## Objectifs

* Travail sur les algorithmes
* Changer l'état du bébé au bout de x temps
* Randomiser ses besoins
