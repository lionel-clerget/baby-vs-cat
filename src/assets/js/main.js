
// Creation 5 class -> Bebe, les besoins, le chrono, les boutons et les sons de l'interface
class Baby {
    constructor(elem) {
        this.data = {
            "mood": {'happy':0, 'cry':1, 'pleased':2},
            "faces": [["assets/images/faces/smile.png", "assets/images/faces/cry.png", "assets/images/faces/nipple.png"], ["assets/images/cats/happy.png", "assets/images/cats/cry.png", "assets/images/cats/love.png"]],
            "sound" : [['assets/sounds/happy.ogg', 'assets/sounds/cry.ogg'], ['assets/sounds/cat_happy.mp3', 'assets/sounds/cat_cry.mp3']]

        };
        this.elem = elem;
        this.type = 0;
        this.state = 'happy';
        this.sound = new Audio();
    }

    changeFace(strMood, isPlayAudio){
        let data = this.data;
        if (strMood == 'pleased') {
            this.state = 'happy';
        }else {
            this.state = strMood;
        }
        this.elem.src = data["faces"][this.type][data["mood"][strMood]];

        this.switchAnimation(this.state, false);

        if (isPlayAudio) {
            this.playSound(this.state);
        }
    }

    switchAnimation(newAnim, isNextAnim, otherAnim){
        this.elem.classList.remove('happy', 'cry', 'enter-exit');
        this.elem.classList.add(newAnim);
        if (isNextAnim) {
            setTimeout(() => {
                this.elem.classList.remove(newAnim);
                this.elem.classList.add(otherAnim);
            }, 1500)
        }
    }

    switchType(currState){
        switch (this.type) {
            case 0:
                this.type = 1;
                break;
            default:
                this.type = 0;
        }
        this.elem.src = this.data["faces"][this.type][this.data["mood"][currState]];
    }

    playSound(strMood){
        this.sound.src = this.data["sound"][this.type][this.data["mood"][strMood]];
        this.sound.play();
    }
}

class Need {
    constructor(elem) {
        this.data = {
            "needs" : ['faim', 'couche', 'intero','jouer'],
            "imgSrc" : ['assets/images/thoughts/hamburger.png', 'assets/images/thoughts/poo.png', 'assets/images/thoughts/intero.png', 'assets/images/thoughts/ball.png']
        }
        this.elem = elem;
        this.currentNeed = '';
        this.nbNeeds = this.data['needs'].length;
        this.isVisible = false;
    }

    setRandNeed(intRand){
        if (intRand <= this.data["imgSrc"].length) {
            this.currentNeed = this.data["needs"][intRand];
            this.elem.src = this.data["imgSrc"][intRand];
            this.elem.classList.add("item-bounce", "visible");
            this.isVisible = true;
        }
    }

    reset(){
        this.elem.classList.remove('item-bounce', 'visible');
        this.isVisible = true;
    }
}

class Chrono {
    constructor(elem) {
        this.elem = elem;
        this.isAlertPlay = false;
        this.sound = new Audio('assets/sounds/tick-tock.mp3');
    }

    setTime(newTime){
        this.elem.innerHTML = newTime;
        if (newTime <= 3) {
            if (!this.isAlertPlay) {
                this.isAlertPlay = true;
                this.sound.loop = true;
                this.sound.play();
                this.elem.classList.add('alert');
            }
        }else {
            this.isAlertPlay = false;
        }
    }

    resetAlert(){
        this.isAlertPlay = false;
        this.sound.pause();
        this.elem.classList.remove('alert');
    }
}

class Buttons {
    constructor(elems) {
        this.data = {
            "text" : [['Donner à manger', 'Changer la couche', 'Jouer'], ['Donner à manger', 'Changer la littière', 'Jouer']]
        }
        this.elems = elems;
        this.isVisible = false;
    }

    grow(){
        let visible = this.isVisible;
        this.elems.forEach(function(elem){
            if (! visible) {
                elem.classList.add('default-size')
            }else {
                elem.classList.remove('default-size');
            }
        });
        this.isVisible = !visible;
    }

    randOrder(){
        return Math.floor(Math.random() * ((this.elems.length+1) - 1) + 1);
    }

    changeOrder(){
        let nbButton = this.elems.length;
        let arr = [this.randOrder()];
        let i = 1;
        let apply = true;
        while (i != nbButton) {
            let rand = this.randOrder();
            for (let j = 0; j < arr.length; j++) {
                if (arr[j] == rand) {
                    apply = false;
                    rand = this.randOrder();
                }
            }
            if (apply) {
                arr.push(rand);
                i ++;
            }
            apply = true
        }
        for (let k = 0; k < nbButton; k++) {
            this.elems[k].style.order = arr[k];
        }
    }

    reset(babyType){
        this.isVisible = false;
        let text = this.data;
        this.elems.forEach(function(elem){
            elem.classList.remove('true', 'false', 'default-size');
            if (elem.hasAttribute('data-faim')) {
                elem.innerHTML = text['text'][babyType][0];
            }
            if (elem.hasAttribute('data-couche')) {
                elem.innerHTML = text['text'][babyType][1];
            }
            if (elem.hasAttribute('data-jouer')) {
                elem.innerHTML = text['text'][babyType][2];
            }
        });
        this.changeOrder();
    }
}

class UISound {
    constructor() {
        this.data = {
            "sound": {'play':0, 'win':1, 'loose':2, 'bonus':3, 'yay':4, 'over':5},
            "src": ['assets/sounds/ui_play.mp3', 'assets/sounds/ui_win.mp3', 'assets/sounds/ui_loose.mp3', 'assets/sounds/ui_bonus.mp3', 'assets/sounds/ui_yay.mp3', 'assets/sounds/game_over.mp3']

        }
        this.elem = new Audio();
    }

    playSound(strSound){
        this.elem.src = this.data["src"][this.data["sound"][strSound]];
        this.elem.play();
    }
}
// FIN CLASS

//Déclaration variables
var baby, need, uiSound, myTimer, sMenu, nextEvent, points, bonus, ceilBonus, bonusIsUsed,  isGameOver;
var eTYPE = {BABY:0, CAT:1};

// Fonctions générales
function getById(id) {
    return document.getElementById(id);
}

function getRandInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min);
}

function changePoints( elemName, isScreenMenu) {
    var p = getById(elemName);
    if (isScreenMenu) {
        p.innerHTML = ''+points+' pts';
    }else {
        p.innerHTML = ''+points;
    }
}


document.addEventListener("DOMContentLoaded", function(event) {
    init();
    UIkit.util.on('#modal-center', 'hide', function () {
        baby.state = 'wait';
        setTimeout(chooseState, 700);
        myTimer = setInterval(timer, 1000);
    });
});

// Initialisation des classes et variables
function init() {
    baby = new Baby(getById('face'));
    need = new Need(getById('need'));
    chrono = new Chrono(getById('chrono'));
    buttons = new Buttons(document.querySelectorAll('button[data-button]'));
    uiSound = new UISound();
    sMenu = getById('screen');
    getById('screenPoints').style.display = 'none';

    nextEvent = 10;
    chrono.setTime(nextEvent);
    bonus = false;
    bonusIsUsed = false;
    ceilBonus = 2;
}

// onclick boutton play
// reset les points et lance le timer
function playNewGame() {
    points = 0;
    changePoints('menuPoints', false);
    isGameOver = false;
    screenMenu(false);
    uiSound.playSound('play');
    setTimeout(() => {
        getById('screenPoints').style.display = 'block';
        baby.changeFace('happy', true);
        myTimer = setInterval(timer, 1000);
    }, 700);
}

// timer déclanché toutes les secondes
// lance les fonctions, check l'etat du bebe, les points...
function timer() {
    if (baby.state == 'happy') {
        if (points == ceilBonus && bonus == false) {
            bonus = true;
            setTimeout(changeBonusState, 1000);
            clearInterval(myTimer);
        }
        else {
            setTimeout(chooseState, 2000);
        }
        baby.state = "wait";
    }
    if (baby.state == 'cry') {
        if (nextEvent > 0) {
            nextEvent --;
            chrono.setTime(nextEvent);
        }else {
            gameOver();
        }
    }
}

// Etat menu principal
function screenMenu(show) {
    if (show) {
        sMenu.setAttribute('style', "animation-name:slideIn; top:70px;")
        changePoints('screenPoints', true);
    }else {
        sMenu.setAttribute('style', "animation-name:slideOut; top:-100vh;");
    }
}

// Choix random des besoins du bebe
function chooseState() {
    baby.changeFace('cry', true);
    need.setRandNeed(getRandInt(0, need.nbNeeds));
    buttons.reset(baby.type);
    buttons.grow()
}

// onclick bouttons besoins -> bonne ou mauvaise réponse
function needClicked(butProp, elem) {
    if (baby.state == 'cry') {
        if (butProp == need.currentNeed) {
            elem.classList.add('true');
            if (butProp == 'intero') {
                baby.state = 'pleased'
            }else {
                baby.state = 'happy'
            }
            winPoint();
        }else {
            elem.classList.add('false');
            uiSound.playSound('loose');
            baby.switchAnimation('wobble', true, 'cry');
        }
    }
}

function winPoint() {
    uiSound.playSound('win');
    baby.changeFace(baby.state, true);
    resetToDefault();
    points ++;
    if (baby.type == eTYPE.CAT) {
        points ++;
    }
    changePoints('menuPoints', false);
    setTimeout(() =>{ buttons.grow()}, 1500)
}

function gameOver() {
    clearInterval(myTimer);
    isGameOver = true;
    uiSound.playSound('over');
    resetToDefault();
    let title = getById('title');
    title.innerHTML = 'Game Over!<br/>Votre Score:<br/>';
    screenMenu(true);
    buttons.reset(baby.type);
    setTimeout(() => {baby.changeFace('happy', false)}, 800);
}

function resetToDefault() {
    nextEvent = 10;
    chrono.resetAlert();
    chrono.setTime(nextEvent);
    need.reset();
}

// Quand le joueur obtient le bonus
function changeBonusState(){
    getById('box-bonus').setAttribute('style', "left:20px;");
    switchBonusImg(true);
    setTimeout(() => {
        uiSound.playSound('bonus');
    }, 550);
    setTimeout(() => {
        UIkit.modal(getById('modal-center')).show();
        baby.state = 'happy';
    }, 1000);

}

// Inverse l'image du bouttons bonus
function switchBonusImg(isFirst){
    let src = ['assets/images/cats/happy.png', 'assets/images/faces/smile.png']
    let b = getById('bonus');
    if (isFirst) {
        b.src = src[0];
        b.classList.add('tada');
    }else {
        b.src = src[baby.type];
        if (b.classList.contains('tada')) {
            b.classList.remove('tada');
        }
    }
}

// onclick boutons bonus
function getBonus() {
    if (bonus == true && baby.state == 'cry' && nextEvent > 3 && bonusIsUsed == false) {
        bonusIsUsed = true;
        baby.state = 'wait';
        buttons.grow();
        need.reset();

        changeBg();
        uiSound.playSound('yay')
        baby.sound.pause();
        baby.switchAnimation('enter-exit');
        setTimeout(() => {
            baby.switchType('cry');
            switchBonusImg(false);
        }, 700);
        setTimeout(() => {
            baby.switchAnimation('cry');
            baby.state = 'cry';
            chooseState();
            bonusIsUsed = false;
        }, 1000);
    }
}

function changeBg() {
    let body = document.querySelector('body');
    body.classList.toggle('baby');
    body.classList.toggle('cat');
}
