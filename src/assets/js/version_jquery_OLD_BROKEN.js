var data = {
    "screen": 'GAME OVER! Votre Score:',
    "faces": [["assets/images/faces/cry.png", "assets/images/faces/smile.png"], ["assets/images/cats/cry.png", "assets/images/cats/happy.png"]],
    "prob" : ['faim', 'couche', 'intero'],
    "text" : [['Donner à manger', 'Changer la couche', 'Calin'], ['Donner à manger', 'Changer la littière', 'Calinou']],
    "item" : ['assets/images/thoughts/hamburger.png',   'assets/images/thoughts/poo.png',   'assets/images/thoughts/intero.png'],
    "sound" : [['assets/sounds/cry.ogg','assets/sounds/happy.ogg'], ['assets/sounds/cat_cry.wav','assets/sounds/cat_happy.wav']],
    "ui" : ['assets/sounds/ui_play.wav', 'assets/sounds/ui_win.wav', 'assets/sounds/ui_loose.wav', 'assets/sounds/ui_bonus.wav', 'assets/sounds/ui_yay.wav', 'assets/sounds/game_over.wav'],
    "bg" : ['../images/bg/kawai1.png', '../images/bg/kawai2.png']

}
var myTimer, face, state, need, type,nextTimeEvent, bonus, gameOver;

// Variable de type Enum pour nommer certaines cellules de mon tableau
var eSTATE = {CRY: 0, HAPPY:1};
var eTYPE = {BABY:0, CAT:1};
var eUI = {PLAY:0, WIN:1, LOOSE:2, BONUS:3, YAY:4, OVER:5}

var points = 0;
var sound = new Audio();
var bonus = false;
var butGrow = false;


function getRandInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min);
}

function init() {
    clearInterval(myTimer);
    if (bonus == false) {
        type = 0;
        nextTimeEvent = 10;
    }
    if (gameOver) {
        points = 0;
        changePoints('#points', points, true);
    }
    changeSound('', true)
    face = "";
    state = "happy";
    need = "none";
    gameOver = false;
    changeFace(true);
    showScreen(false);
    showTimer(true, false);
    changeOrder();
    myTimer = setInterval(timer, 1000);
}

function timer(){
    if (state == "happy") {
        setTimeout(chooseState, 2000);
        state = "wait";
    }
    if (state == "cry"){
        if (nextTimeEvent > 0) {
            nextTimeEvent -= 1;

            if (nextTimeEvent > 3) {
                showTimer(true, false);
            }else {
                showTimer(true, true);
            }
        }else {
            if (! gameOver) {
                showTimer(false);
                showScreen(true);
                setGameOver();
                gameOver = true;
            }
        }
    }
    if (points > 1 && bonus == false) {
        bonus = true;
        changeBonusState();
    }
}

function showScreen(show) {
    if (show) {
        $('#screen').css({"animation-name": "slideIn", "top" : "70px"});
        changePoints('#mypoints', points, false);
    }else {
        $('#screen').css({"animation-name": "slideOut", "top" : "-100vh"});
    }

}

function showTimer(show, alert) {
    var t = $('#timer');
    if (show) {
        t.text(''+nextTimeEvent);
        if (alert) {
            t.addClass('alert')
        }
        else {
            t.removeClass('alert')
        }
    }else {
        t.text('');
        t.removeClass('alert')
    }
}

function changePoints( elemName, nb, isMenu) {
    var p = $(elemName);
    if (isMenu) {
        p.text(''+points);
    }else {
        p.text(''+points+' pts');
    }
}

function chooseState() {
    resetButton();
    changeFace(false);
    changeItem(true);
}

function changeFace(isReset) {
    var face = $("#face");
    var src = face.attr('src');

    if (! isReset) {
        switch (src) {
            case data["faces"][type][eSTATE.HAPPY]:
                face.attr('src', data["faces"][type][eSTATE.CRY]);
                face.addClass('face-rotate');
                state = 'cry';
                changeSound(state, false)
                break;
            default:
                face.attr('src', data["faces"][type][eSTATE.HAPPY]);
                face.removeClass('face-rotate');
                $('#item').removeClass('item-bounce')
                state = 'happy';
                changeSound(state, false)
        }
    }else {
        face.attr('src', data["faces"][type][eSTATE.HAPPY]);
        face.removeClass('face-rotate');
        $('#item').removeClass('item-bounce');
        $('#item').removeClass('visible');
        state = 'happy';
    }
}

function changeItem(isInit) {
    var item = $('#item');
    if (isInit) {
        var rand = Math.floor(Math.random() * 3);
        need = data["prob"][rand];
        state = 'cry';

        item.attr('src', data["item"][rand]);
        item.addClass('item-bounce');
        growButton();
    }
    if (item.hasClass('visible')) {
        item.removeClass('visible');
    }else {
        item.addClass('visible');
    }
}

function changeSound(_state, isUI) {
    if (! isUI) {
        switch (_state) {
            case 'cry':
                sound.src = data["sound"][type][eSTATE.CRY];
                break;
            default:
                sound.src = data["sound"][type][eSTATE.HAPPY];
        }
    }else {
        switch (_state) {
            case 'win':
                sound.src = data["ui"][eUI.WIN];
                break;
            default:
                sound.src = data["ui"][eUI.PLAY];
        }
    }
    sound.play()
}

function give(_button, elem){
    var s = new Audio();
    if (_button == need && state == 'cry') {
        s.src = data["ui"][eUI.WIN];
        $(elem).addClass('true');
        changeFace(false);
        changeItem(false);
        nextTimeEvent = 10;
        points ++;
        if (type == eTYPE.CAT) {
            points ++;
        }
        changePoints('#points', points, true);
        setTimeout(growButton, 700)

    }else {
        $(elem).addClass('false');
        s.src = data["ui"][eUI.LOOSE];
    }
    s.play();
}

function resetButton() {
    $("#answer").find('button').each(function() {
        if ($(this).hasClass('true')) {
            $(this).removeClass('true');
        }
        if ($(this).hasClass('false')) {
            $(this).removeClass('false');
        }

        switch ($(this).attr('name')) {
            case 'faim':
                $(this).text(data["text"][type][0]);
                break;
            case 'couche':
                $(this).text(data["text"][type][1]);
                break;
            default:
                $(this).text(data["text"][type][2]);

        }
    });
    changeOrder();
}

function growButton() {
    $("#answer").find('button').each(function() {
        if ($(this).hasClass('default-size')) {
            $(this).removeClass('default-size');
            butGrow = false;
        }else{
            $(this).addClass('default-size');
            butGrow = true;
        }
    });
}

function changeOrder() {
    var butArray = $("#answer").find('button');
    var nbButton = butArray.length;
    var arr = [getRandInt(1,4)];
    var i = 1;
    while (i != nbButton) {
        var rand = getRandInt(1,4);
        var apply = true;
        for (var j = 0; j < arr.length; j++) {
            if (arr[j] == rand) {
                apply = false;
                rand = getRandInt(1,4);
            }else {
                apply = true;
            }
        }

        if (apply) {
            arr.push(rand);
            i += 1;
        }
    }
    for (var k = 0; k < butArray.length; k++) {
        butArray[k].style.order = arr[k]
    }
}

function getBonus() {
    if (bonus == true && state == 'cry' && nextTimeEvent > 3) {
        if (type == eTYPE.BABY) {
            type = eTYPE.CAT;
        }else {
            type = eTYPE.BABY;
        }
        $('body').toggleClass('baby');
        $('body').toggleClass('cat');

        var s = new Audio(data["ui"][eUI.YAY]);
        s.play();

        var b = $('#bonus');
        b.attr('src', data["faces"][eTYPE.BABY][eSTATE.HAPPY]);
        b.removeClass('tada');
        init();
        if (butGrow) {
            growButton();
        }
    }
}

function changeBonusState() {
    var s = new Audio(data["ui"][eUI.BONUS]);
    s.play();
    var b = $('#bonus');
    b.attr('src', data["faces"][eTYPE.CAT][eSTATE.HAPPY]);
    b.addClass('tada');

}

function setGameOver(){
    var s = new Audio(data["ui"][eUI.OVER]);
    s.play();
    nextTimeEvent = 10;
    state = "wait";
    var t = $('#title');
    t.text(data["screen"]);
    growButton();
}

// $( document ).ready(function() {
//     init();
// });
